Generated Chess checkmate positions

 -for chess engines testing, long notation (Ng1-f3) .pgn files
 -single first move solutions with minimal pieces on board

The project is at https://github.com/Chessforeva/Cpp4chess / CM
