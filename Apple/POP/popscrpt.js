/*
POP related additions,
touch buttons, long loading of disks
*/

window.oncontextmenu = function(event) {
     event.preventDefault();
     event.stopPropagation();
     return false;
}; 

function GE(g) { return document.getElementById(g) }

function Dsp(m) { GE("statuss").innerHTML = m; }

function ShowOptKeyb()
{
var d = GE("OPTSKEYB"), s = d.style;
s.visibility="visible";
s.position="relative";
s.top="0px";
s.width="560px";

d = GE("OPTBUT_"); d.innerHTML = '';
}

function LoadPOP()
{
var cksm = screenchecksum();
if(!cksm) { setTimeout('LoadPOP()',999); return; }	// wait for "Apple //e" (ready)

updateCPU(1);
updateJoystick(1);
turnSound();

setTimeout('readyPOP()',999);
Dsp("Wait, loading...");
myLoads();
}

DISKALOADED = 0;
CKICKSTAT = 0;
SOUNDENABLED = 1;

function turnSound()
{
SOUNDENABLED^=1;
enableSound(SOUNDENABLED);
}

function readyPOP()
{
var name = "Prince Boot";	    
disk2.setBinary(1,name, "dsk", princeofpersiaboot);
drivelights.label(1,name);

setTimeout('screenckr()',999);
Dsp("Booting, do nothing");
}

LASTcksm = 0;
TCKcksm = 0;
TCKwait = 0;

function screenckr()
{
  var cksm = screenchecksum();
  
  if(!CKICKSTAT && cksm)
	{
	setTimeout('kliks()',3000);	// click to start
	setTimeout('kliks()',6000)	// ..to skip intro
	CKICKSTAT=1;
	}  
  
  TCKwait++;
  TCKcksm++; 
  if(cksm!=LASTcksm) { LASTcksm=cksm; TCKcksm = 0; }
  //console.log(cksm);

  // when see "Please insert side A" or after 15 seconds
  if(!DISKALOADED)
	{
	if(TCKwait>60 || (TCKcksm>5 && cksm && cksm<9999))
		{
		DISKALOADED=1; SwapDisks();
		setTimeout('kliks()',999);
		setTimeout('kliks()',2000);
		setTimeout('waitBrodb()',2000);
		Dsp("Loading disks, do nothing");
		}
	else setTimeout('screenckr()',333);
	}

}

function SwapDisks()
{
 DISKALOADED = (DISKALOADED==3 ? 1 : DISKALOADED+1);
 var a=DISKALOADED;
 var name=(a==1 ? "Prince Boot" : (a==2 ? "Prince A" : "Prince B"));
 var disk=(a==1 ? princeofpersiaboot : (a==2 ? princeofpersiaa : princeofpersiab));
 disk2.setBinary(1,name, "dsk", disk);
 drivelights.label(1,name);
}

function waitBrodb()
{
   var cksm = screenchecksum();
   if(cksm)
	{
	setTimeout('kliks()',2000);
	setTimeout('kliks()',3000);
	setTimeout('kliksArrowKey()',6000);	// arrow key to start ok.
	Dsp("Still do nothing, skipping title");
	}
   else setTimeout('waitBrodb()',333);
}

function screenchecksum()
{
 var C = GE("screen");
 if(C==null) return 0;
 var cx = C.getContext('2d'), w = C.width, h = C.height;
 var I = cx.getImageData(0, 0, w, h), D = I.data;
 var dx = ((w/560)*8)|0, dy = ((h/384)*8)|0;  
 var y,x,i, cksm = 0;
 for(y=0;y<h;y+=dy)
  for(x=0;x<w;x+=dx)
   {
   i = (x+(y*w))<<2;
   cksm += ( D[i] | D[i+1] | D[i+2] );
   }
 return cksm;
}

var EVEnt = { keyCode:0, shiftKey:0, ctrlKey:0, preventDefault:function(){} };

function pushkey(c)
{
EVEnt.keyCode = c;
_keydown(EVEnt);
}
function liftkey(c)
{
EVEnt.keyCode = c;
_keyup(EVEnt);
}


function kliks()
{
 var cksm = screenchecksum();
 if(!cksm) setTimeout('kliks()',300);
 else pushkey(32);
}

function kliksArrowKey()
{
 var cksm = screenchecksum();
 if(!cksm) setTimeout('kliksArrowKey()',300);
 else { pushkey(38); setTimeout('setnewkeyroutes()',999); }
}

function setnewkeyroutes()
{
if(document.onkeydown==null)
 {
  window.onkeydown = _popkeydown;
  window.onkeyup = _popkeyup;
 }
else
 {
 document.onkeydown = _popkeydown;
 document.onkeyup = _popkeyup;
 }
Dsp("Prince should be ready, or try reload page.");
setTimeout('Abouts()',4000);
setInterval('KlikCtrlK()',4000);
}

// return keyboard mode
function KlikCtrlK()
{
 pushKey(2075);
}

function Abouts()
{
 var s='Thanks to '+
	'<a href="http://www.popuw.com/apple.html" target="blank">POP</a> and '+
	'<a href="http://www.scullinsteel.com/apple//e" target="blank">emulator</a>';
	
 s+='<br> Cheats: ' +
	'<input type="button" value="Health++" onclick="Cheat_Health_plus()">'+
 	'<input type="button" value="Level++" onclick="Cheat_Level_plus()">';
 Dsp(s);
 GE("Swapdsk").style.visibility="visible";
 
}

// Let's make it playable :)

function keysubstit(e,t)
{
var a = e.keyCode, c=0, f=0, m=0;

if(a==13) a=2075;	// Enter activates keyboard too

if(a>2000) { m=1; a-=2000; c=a; }
 
if(a==37) c=74;	// left J
if(a==38) c=73;	// jump up I
if(a==39) c=76;	// right L
if(a==40) c=75;	// down = K
if(a==90) c=85;	// jump left Z = U
if(a==88) c=79;	// jump right X = O

if(a==300 && t) turnSound();

if(a>1000) c=a-1000;

if(a==188) { c=74; f=1; }	// Alt+J = <
if(a==190) { c=76; f=1; }	// Alt+L = >
if(a==191) { c=75; f=1; }	// Alt+K = ?

if(c)
 {
 e.preventDefault();
 if(f)	// Alt keypress
  {
   if(t) pushkey(18); else liftkey(18);
  }
 
 if(m)	// Ctrl keypress
  {
   if(m) EVEnt.ctrlKey = 1;
  }

 if(t) pushkey(c); else liftkey(c);
 if(m) EVEnt.ctrlKey = 0;
 return 1;
 }
 return 0;
}

function _popkeydown(e)
{
if(!keysubstit(e,1)) _keydown(e);
}

function _popkeyup(e)
{
if(!keysubstit(e,0)) _keyup(e);
}

//=====

TOUCH_ = ('ontouchstart' in document.documentElement);
MOBILE_ = (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i));

KBUT_ = [];

// keyboard on buttons
// touch things

var EVENT = { type: "", keyCode: 0,
 preventDefault: function() {} , stopPropagation: function() {} };

function keyAction(t,k)
{
focused = 0;
EVENT.keyCode = k;
EVENT.type = t;
EVENT.which = k;
if(t=="keydown")
 {
 if(!keysubstit(EVENT,1)) pushkey(k);
 }
if(t=="keyup")
 {
 if(!keysubstit(EVENT,0)) liftkey(k);
 }
}
			
				
function AddKeyButtons()
{        
    addKey( "Left",0, 37,  630,220, 80,160,'J' );
    addKey( "Up",0, 38,  720,220, 80,80,'I' );
    addKey( "Down",0, 40,  720,300, 80,80,'K' );
    addKey( "Right",0, 39,  810,220, 80,160,'L' );
    
    addKey( "Esc",0, 27,  630,60, 60,60,'pause' );	// Esc - pause
    addKey( "Space",0, 32,  730,60, 40,40,'time' );	// Space - time left
    addKey( "Space",0, 300,  800,60, 40,40,'sound' );	// Sound
    
    addKey( "Enter",0, 2075,  770,105, 60,40,'keyb.<br>ctrl+K' );	// Ctrl+K

     
    addKey( "Space",0, 2065,  690,10, 40,40,'reload<br>level<br>ctrl+A' );	// Ctrl+A
    addKey( "Space",0, 2082,  800,10, 40,40,'reload<br>title<br>ctrl+R' );	// Ctrl+R

    // Jumps
    addKey( "Left",0, 1085,  650,150, 60,60,'U<br>Z' );
    addKey( "Right",0, 1079,  810,150, 60,60,'O<br>X' );
    addKey( "Alt",0, 1018,  730,150, 60,60,'' );
    
    // Alt+...
    addKey( "Left",0, 188,  650,390, 60,60,'<' );
    addKey( "Right",0, 190,  810,390, 60,60,'>' );
    addKey( "Down",0, 191,  730,390, 60,60,'?' );
}

KBF_ = [];
function keyLifter()
{
var i,o;
for(i in KBF_)
 {
  o = KBF_[i];
  o.i++;
  if(o.i==1) keyAction("keypress",o.c);
  if(o.i>=5)
   {
    keyAction("keyup",o.c);
    KBF_.splice(i,1);
   }
 }
 setTimeout('keyLifter()',20);
}
if(!TOUCH_) keyLifter();

function pushKey(c)
{
var i,o,y=1;
for(i in KBF_)
 {
  o = KBF_[i];
  if(o.c==c) { y=0; break; }
 }
if(y)
 {
  keyAction("keydown",c);
  KBF_.push( { i:0, c:c } );
 }
else o.i=0;
}

function releaseKey(c)
{
var i,o;
for(i in KBF_)
 {
  o = KBF_[i];
  if(o.c==c)
   {
    keyAction("keyup",o.c);
    KBF_.splice(i,1);
   }
 }
}

function addKey(n,f,c,x,y,w,h,t)
{
 var s = '<div style="position:absolute; left:'+x+'px;top:'+y+'px">' +
  '<img id="kButt'+c+'" src="/keybimgs/'+(f?f:n)+'.png" width="'+w+'" height="'+h+'" alt="'+n+'" >' +
  '</div>';
 KBUT_.push(c);
 s+='<div style="position:absolute; left:'+(x+w+2)+'px;top:'+(y+4)+'px">' +
  '<font size="1">'+t+'</font></div>';
 document.write(s);
}

function DummyEv(e) { e.stopPropagation(); e.preventDefault(); return false; } 

function myLoads()
{
 var O,i;
 for(i in KBUT_)
  {
   O = GE("kButt"+KBUT_[i]);
   if(TOUCH_)
   {
   O.addEventListener("touchstart", TouchClick, false);
   O.addEventListener("touchmove", DummyEv, false);
   O.addEventListener("touchend", TouchUp, false);
   }
  else
   {
   O.addEventListener("mousedown", MouseClick, false);
   }
   O.style.zIndex = 9999;
  }
}


function MouseClick(e)
{
 TouchClick(e);
}

function TouchClick(e)
{
e.stopPropagation(); e.preventDefault();

var t = e.target; if(!t) t=e.currentTarget;
pushKey( parseInt(t.id.substr(5)) );
} 


function TouchUp(e)
{
e.stopPropagation(); e.preventDefault();
var t = e.target; if(!t) t=e.currentTarget;
releaseKey( parseInt(t.id.substr(5)) );
} 

AddKeyButtons();


function Cheat_Health_plus()
{
__RAM[0][1].ram[206]=12;	//718
}

function Cheat_Level_plus()
{
 __RAM[1][1].ram[284]++;	//1820
}
