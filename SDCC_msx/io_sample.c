#include "solidc\CONIO.h"
#include "solidc\IO.h"

char __at (0x9A00) s1[] = "Hello  \0";
char __at (0x9B00) s2[] = "world of ...!\n\0";
char __at (0x9BF0) s3[] = "MSX\0";
char __at (0x9C00) sn[] = "\n\0";
char __at (0x9D00) sbuf[0xff];

#define aPoi 0x9E00
long __at (aPoi) poi = 0;	// this way we pass long variable to ASM as simple pointer

void main(void)
{
 FCBlist *FCB = FCBs();		// obligatory!
 int fH,fH2,n;
 
 //_os_ver = 1;		// set manually for file operations
			// MSXDOS2 is MSXDOS1 backwards compatible, FCB works for sure

	// displays A:
 getcwd(sbuf, 0xFF);
 cputs(sbuf); cputs(sn);
 
	// create a text file with "Hello  " string
 fH = create( "B.TXT" );
 write( fH, s1, 7 );
	// make "Hello" to "HeLLo" in file
 poi = 2; lseek(fH, aPoi, SEEK_SET);
 write( fH, "LL\0", 2 );
 close(fH);
 
	// append "world of ...!" to the file
 fH2 = open( "B.TXT", O_APPEND );
 write( fH2, s2, 15 );
 	// put "MSX" inside in two ways
 if(_os_ver>1)  { poi = -6; lseek(fH2, aPoi, SEEK_END); } else _seek(fH2,-6,SEEK_END);
 write( fH2, s3, 3 );
 close(fH2);

	// read file and display on screen
 fH = open( "B.TXT", O_RDWR );
 
 poi = 5; lseek(fH, aPoi, SEEK_SET);
 write( fH, ",", 1 );	// add comma

 poi = 0; lseek(fH,aPoi,SEEK_SET);
 read( fH, sbuf, (unsigned int)_size(fH));
 close(fH);
 cputs(sbuf); cputs(sn);
 
	// rename our file
 rename("B.TXT","A.TXT");
  
	// update version number _os_ver, try folder functions
 if(get_OS_version()>1)
  {
   mkdir("MYFLDR");	// create
   chdir("MYFLDR");	// set to folder
   getcwd(sbuf, 0xFF);		// displays "MYFLDR"
   cputs(sbuf); cputs(sn);
   chdir("\\");		// set root
   getcwd(sbuf, 0xFF);		// displays ""
   cputs(sbuf); cputs(sn);
   rmdir("MYFLDR");	// remove
  }

 fH = create( "D.TXT" );	// let's create a test file
 write( fH, s1, 7 ); 
 close(fH);
 remove( "D.TXT" );	// and remove it
 
	// display all files in folder
 cputs("Files in folder:\n\0");
 n=findfirst("*.*",sbuf,0);
 for(;!n;)
  {
  cputs(sbuf); cputs(sn);
  n=findnext(sbuf);
  }

 // set from A: to B:
 // n = getdisk(); putdec(n); setdisk(n+1);
}
