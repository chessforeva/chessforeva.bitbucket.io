#include "solidc\VDPgraph2.h"

#define	_pt_(h,v) psetXY(X+(h),Y+(v),colour)
#define	_ln_(h,v) LINE(X+(h),Y+(v),X-(h),Y+(v),colour,0)
#define	pt_(h,v) if(OP==0xff)_ln_(h,v);else if(OP!=0xff)_pt_(h,v)

// Draws circle, fills if OP is FILL_ALL.
extern	void	CIRCLE( int X, int Y, int Radius, int colour, int OP )
	{
	int x = Radius; int y = 0; int e = 0;
	while(1)
		{
		pt_(+x,+y); pt_(+x,-y); pt_(-x,+y); pt_(-x,-y);
		pt_(+y,+x); pt_(+y,-x); pt_(-y,+x); pt_(-y,-x);						
		if (x <= y) break;
		e += (y<<1) + 1; y++;
		if (e > x) { e += 1 - (x<<1); x--; }
		}
	}
	
MMMtask __at(0x2000) t;
char __at (0xBC00) pixeldatas[1400];	// 70x40px (byte=2px)

void main(void)
{

 int c;
 if(vMSX()<2) return;	// only MSX2 and above

 Save_VDP();		// Save VDP internals
 DisableScreen();
 SetScreen5();
 SetFasterVDP();	// optimize VDP, sprites off
 SetColors(WHITE,BLACK,DARK_BLUE);	// white on black with dark blue border
 ClearScreen();
 EnableScreen();
  
	// Put red pixel in right bottom corner
 PSET(255,211,LIGHT_RED,LOGICAL_IMP);

 c = POINT(255,211);	// Read pixel colour = 9
  
 LINE(110,110,160,120,LIGHT_GREEN,LOGICAL_IMP);	// Draw green line
 HMCM(100,100,170,140,pixeldatas);	// Save screen rectangle 70x40 to memory 
 HMMC(pixeldatas, 80,3,70,40);		// Duplicate 70x40 block from memory to (80,3)
 HMMC(pixeldatas, 10,102,70,40 );	// Duplicate 70x40 block from memory to (10,102)
 
 LINE(70,90,130,200,DARK_RED, LOGICAL_TIMP);	// Draw red line
 LINE(12,129,70,90,MAGENTA, LOGICAL_TIMP);	// Draw magenta line
 
 CIRCLE( 30, 30, 20, DARK_BLUE, LOGICAL_TIMP );		// circle by c-drawing
 PAINT( 32, 32, CYAN, DARK_BLUE );			// Paint inside circle
  
 RECT( 20, 160, 80, 208, GRAY, LOGICAL_TIMP );	// 4 gray lines
 
 CIRCLE( 100, 99, 50, MEDIUM_RED, FILL_ALL );	// filled circle
 CIRCLE( 100, 100, 20, DARK_RED, FILL_ALL );	// filled circle inside
 PAINT( 100, 100, LIGHT_GREEN, DARK_RED );	// Paint region
 
 RECT( 206, 30, 250, 200, MEDIUM_GREEN, FILL_ALL );	// Draw rectangle and fill it
 
 // Draw triangle and fill
 LINE(181,99,99,99, DARK_YELLOW ,LOGICAL_IMP);
 LINE(180,100,140,40, DARK_YELLOW ,LOGICAL_IMP);
 LINE(100,100,140,40, DARK_YELLOW ,LOGICAL_IMP);
 PAINT( 140, 80, 15, DARK_YELLOW );
 
 // duplicate in video memory to (160,120) , fast copy way with logical operation
 LMMM(10,60,90,140, 160,120,LOGICAL_TNOT);
 
 DisableScreen();	// User should not see now (freeze)
 
 Read_Scr( 0x5000 );	// Read 0x6A00 bytes to address #0x5000
 
 ClearScreen();		// clear all to see nothing left

 Write_Scr( 0x5000 );	// Write to screen 0x6A00 bytes from address #0x5000
 
 EnableScreen();	// Display to user now
 
 // Now test far copy
 t.X = 0; t.Y = 0; t.DX = 256;	t.DY = 212; 	// region (0,0)-(255,256) 
 t.X2 = 0; t.Y2 = 3*256;			// to (0,702)
 t.s0 = 0; t.DI = 0; t.LOP = LOGICAL_IMP;
 fLMMM(&t);	// set VDP to copy to page 3, that is outside of visible screen region

 // Let's copy back
 t.X = 0; t.Y = 3*256; t.DX = 256; t.DY = 212;	// region (0,702)-(255,1023) 
 t.X2 = 15; t.Y2 = 5;				// to (15,5) visible
 t.s0 = 0; t.DI = 0; t.LOP = LOGICAL_IMP;
 fLMMM(&t);	// copy back to page 0 (15,5)
 
 SetColors( MEDIUM_RED, BLACK, BLACK ); 
 PutText(160,180,"Hey,\0");
 SetColors( WHITE, BLACK, DARK_BLUE  ); 
 PutText(160,190,"Press ESC to return to MSXDOS A>\0");

 while(Inkey()!=27);	// wait for ESC

 SetScreen0();
 SetColors(WHITE,DARK_BLUE,DARK_BLUE);	// restore default MSX2 colours
 Restore_VDP();		// Restore VDP internals
}

	
