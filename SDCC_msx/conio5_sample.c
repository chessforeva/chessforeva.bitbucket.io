#include "solidc\CONIO.h"
#include "solidc\CONIO5.h"

char __at (0x9000) sbuf[0xff];

void main(void)
{
 int n;

 // wait user input
 n = getscon(sbuf, 255);

 // display
 cputs(sbuf); putchar(13); putchar(10);
 putdec(n);

 cputs(" chars entered, press ESC");
 for(;getchar()!=27;);
}
