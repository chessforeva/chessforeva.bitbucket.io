
#include "solidc\CONIO.h"

char __at (0x9A09) s1[] = "The first\n\0";
char __at (0x9C40) s2[] = "The second...";
char __at (0x9C99) sn[] = "\n\0";
char __at (0x9f00) s3[] = "The third line: \0";
char __at (0x9900) sbuf[0x30];

void main(void)
{

 int min = -32768; 
 int max = 32767;

 sbuf[0]='H';
 sbuf[1]='e';
 sbuf[2]=sbuf[3]='l';
 sbuf[4]='o';
 sbuf[5]=0;

 clrscr();	// clear screen

 cputs( s1 );
 cputs( s2 ); cputs( sn );
 cputs( s3 ); cputs( sbuf ); putch(10);

 putdec( min  ); putch(',');
 putdec( 2153 ); putch(',');
 putdec( max  ); putch(10);

 cputs("press a key: ");
 getche();	// wait for key press and print out

 gotoxy(10,20);	// move cursor to position
 cputs("now Esc: ");

 // loop while no keys in buffer or other than Esc
 while(!kbhit() || getch()!=27);

 // move cursor to left upper corner
 //gohome();

}
