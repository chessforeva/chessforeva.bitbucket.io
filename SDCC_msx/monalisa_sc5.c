#include "solidc\VDPgraph2.h"
#include "solidc\diskload.h"
/*
*	This C source for SDCC compiler
*	loads picture file "MONALISA.SC5"
*	into memory, copies to VRAM
*	Pure MSXDOS level loader without Basic sub-ROM.
*
*   Picture has been prepared by converter tool "BMP to MSX converter HRA!"
*     authors: imasy.or.jp
*

 To load from basic, run this basic program
10 SCREEN 5
20 COLOR 15,0,0
30 BLOAD "monalisa.sc5",S
40 COLOR=RESTORE
50 GOTO 50

*/

/* RGB table of 16 colours for MONALISA.SC5 */
char __at(0xB000) palette[] = {
0, 0,0,0,
1, 2,1,1,
2, 6,5,4,
3, 5,4,3,
4, 5,5,3,
5, 6,5,3,
6, 7,6,4,
7, 3,2,1,
8, 7,5,2,
9, 6,4,2,
10, 4,3,2,
11, 4,3,1,
12, 5,3,2,
13, 3,3,2,
14, 3,1,0,
15, 5,2,0
};

void main(void)
{
 Save_VDP();		// Save VDP internals
 SetScreen5();		// SET SCREEN 5
 SetFasterVDP();	// optimize VDP, sprites off

 SetPage(0);		// Set the main page 0
 SetBorderColor(0);	// Background + border
	// Set palette, also .SC5 contains palette, but we ignore.
	// It is the same as sending the VRAM block 0x7680-0x769F
	//  to the VDP (should acknowledge) after diskloading.
 SetPalette((Palette *)palette);	

 diskload("MONALISASC5",0x5000-7,0);	// ignore 7-bytes .SC5 header (FE+addresses)

 Write_Scr( 0x5000 );	// Write to screen 0x6A00 bytes from address #0x5000

 while(WaitForKey()!=27);	 // ESC key returns to MSXDOS
  
 RestorePalette();	// Set back MSX default palette
 SetScreen0();		// SET SCREEN 0 text mode
 ClearScreen();		// A>_
 Restore_VDP();		// Restore VDP internals
}
