#include "solidc\VDPgraph2.h"

void main(void)
{
 int x, y, xi, yi;
 if(vMSX()<2) return;	// only MSX2 and above
 Save_VDP();		// Save VDP internals
 DisableScreen();
 SetScreen5();
 SetFasterVDP();	// optimize VDP, sprites off
 SetColors(WHITE,BLACK,BLACK);	// white on black with dark blue border
 ClearScreen();
 EnableScreen();

 // draw DONKEY classic (IBM,1981)
 // http://en.wikipedia.org/wiki/DONKEY.BAS
 DRAW("S8C3BM12,6r3m+1,+3d2R1ND2u1r2d4l2u1l1d7R1nd2u2r3d6l3u2l1d3m-1,+1l3m-1,-1u3l1d2l3u6r3d2nd2r1u7l1d1l2u4r2d1nd2R1U2M+1,-3BD10D2R3U2M-1,-1L1M-1,+1BD3D1R1U1L1BR2R1D1L1U1BD2BL2D1R1U1L1BR2R1D1L1U1BD2BL2D1R1U1L1BR2R1D1L1U1\0");
 DRAW("S16C3BM44,28M+2,-4R8M+1,-1U1M+1,+1M+2,-1M-1,+1M+1,+3M-1,+1M-1,-2M-1,+2D3L1U3M-1,+1D2L1U2L3D2L1U2M-1,-1D3L1U5M-2,+3\0");
 PAINT(15,9,MEDIUM_RED,3);
 PAINT(60,20,GRAY,3);
 
 for(xi=0;xi<3;xi++)
  {
  if(xi<2)
   {
   x = 120+(xi*60);
   y = 2;
   LMMM(0,2,32,48, x, y, LOGICAL_TIMP );  // copy car
   }
  for(yi=0;yi<3;yi++)
   {
   x = (xi*90);
   y = 70+(yi*40);
   LMMM(42,2,102,40, x, y,LOGICAL_TIMP );	// copy cow
   PAINT( x+20, y+16, 3+xi+yi, 3);			// fill with colour
   }
  }
 
 while(Inkey()!=27);	// wait for ESC

 SetScreen0();
 SetColors(WHITE,DARK_BLUE,DARK_BLUE);	// restore default MSX2 colours
 Restore_VDP();		// Restore VDP internals
}

	
