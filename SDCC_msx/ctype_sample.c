#include "solidc\CONIO.h"
#include "solidc\CTYPE.h"

char __at (0x9910) s1[] = "\0";
char __at (0x9920) s2[] = "(\0";
char __at (0x9930) s3[] = "):\0";
char __at (0x9A00) sn[] = "\n\0";
char __at (0x9B00) m[] = ",\0";	

void main(void)
{
 int i;
 for(i=0;i<160; i+=16)
	{
	cputs(s1); putch( iscntrl(i) ? ' '  : i ); cputs(s2); putdec(i); cputs(s3);
	putch( isprint(i) ? tolower(i) :' ' ); cputs(m);	/* convert to lowercase */
	putch( isprint(i) ? toupper(i) :' ' ); cputs(m);	/* convert to uppercase */
	putch( '0'+isalnum(i) ); cputs(m);	/* A-Za-z0-9 */
	putch( '0'+isalpha(i) ); cputs(m);	/* A-Za-z */
	putch( '0'+isascii(i) ); cputs(m);	/* !..~ */
	putch( '0'+isdigit(i) ); cputs(m);	/* 0..9 */
	putch( '0'+iscntrl(i) ); cputs(m);	/* unprintable control symbol */
	putch( '0'+isprint(i) ); cputs(m);	/* printable test */
	putch( '0'+islower(i) ); cputs(m);	/* lowercase test */
	putch( '0'+isupper(i) ); cputs(m);	/* uppercase test */
	putch( '0'+ispunct(i) ); cputs(m);	/* punctuation sign test */
	putch( '0'+isspace(i) ); cputs(m);	/* space test */	
	putch( '0'+isxdigit(i) );		/* hex digit test */
	cputs(sn);
	}
}