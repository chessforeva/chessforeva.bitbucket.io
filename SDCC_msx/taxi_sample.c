#include "solidc\VDPgraph1.h"
#include "solidc\diskload.h"
/*
*	This C source for SDCC compiler
*	loads picture file "TAXI.SC2"
*	into memory, copies to VRAM
*	Pure MSXDOS level loader without Basic sub-ROM.
*
*   Picture has been prepared by converter tool "BMP to MSX converter HRA!"
*     authors: imasy.or.jp
*

 To load from basic on MSX1, run this basic program
10 SCREEN 2
20 COLOR 15,0,0
30 BLOAD "taxi.sc2",S
40 GOTO 40

*/


void main(void)
{
 Save_VDP();		// Save VDP internals
 SetScreen2();		// SET SCREEN 2

 SetBorderColor(0);	// Background + border	

 diskload("TAXI    SC2",0x5000-7,0);	// ignore 7-bytes .SC2 header (FE+addresses)

 Write_Scr( 0x5000, 0x7000 );	// Write palettes data and colours data to VRAM
 
 while(WaitForKey()!=27);	 // ESC key returns to MSXDOS
  
 SetScreen0();		// SET SCREEN 0 text mode
 ClearScreen();		// A>_
 Restore_VDP();		// Restore VDP internals
}
