#include "solidc\VDPgraph2.h"

// one of [0..255] patterns
#define nrPattern1 #0
char __at (0x9000) mySprite1Pattern[]= {
	0b11111111,
	0b11111111,
	0b11000011,
	0b11011011,
	0b11011011,
	0b11000011,
	0b11111111,
	0b11111111
};

// one of [0..32] sprites made of patterns
#define nrSpr1 #0

char __at (0x9008) mySprite1LineColours[]= {
	MEDIUM_RED,
	LIGHT_RED,
	DARK_YELLOW,
	LIGHT_YELLOW,
	DARK_GREEN,
	MAGENTA,
	GRAY,
	WHITE
};

#define nrSpr2 #0
#define nrPattern2 #0

unsigned int __at (0x9010) mySprite2Pattern[]= {
	0b1111111111111111, 
	0b1111111111111111,
	0b1100000000000011,
	0b1100000000000011,
	0b1100000110000011,
	0b1100001111000011,
	0b1100011111100011, 
	0b1100011111100011,
	0b1100111111110011, 
	0b1100011111100011,
	0b1100001111000011,
	0b1100000110000011,
	0b1100000000000011,
	0b1100000000000011,
	0b1111111111111111, 
	0b1111111111111111
};
	// all the colours of palette, 4 lines left shifted
char __at (0x9030) mySprite2LineColours[]= { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 };

#define nrSpr3 #1			// n - sprite number
#define nrPattern3 #4			// (n*4) palette number for sprite(-es) 
char __at (0x9040) mySprite3Pattern[]= {	// created on http://msx.jannone.org/tinysprite/
	0x07,0x0F,0x05,0x0F,0x07,0x03,0x0F,0x3F,
	0x7F,0xE7,0xC7,0x0F,0x1E,0x1C,0x3C,0x7C,
	0xE0,0xF0,0xA0,0xF0,0xE0,0xC0,0xF0,0xFC,
	0xFE,0xE7,0xE3,0xF0,0x78,0x38,0x3C,0x3E
};
char __at (0x9060) mySprite3LineColours[]= { 6,6,10,10,10,10,8,8,8,8,8,10,12,12,12,1 };

void main(void)
{
 int x, y, dx, dy, i, k, xi, yi, gx, gy;
 if(vMSX()<2) return;	// only MSX2 and above
 Save_VDP();		// Save VDP internals
 SetScreen5();
 SpritesDoubleSized();
 Sprites_On();
 EnableScreen();
 /*
 Sprites_8();
 SpritePattern( nrPattern1, mySprite1Pattern );		// define new points set
 SpriteColours( nrSpr1, mySprite1LineColours );		// set colours for our sprite lines (default is WHITE)
 SpriteAttribs( nrSpr1, nrPattern1, 40, 120 );		// Put a sprite to (40,120), linked to pattern
 PutText(0,176,"Press ESC\0");
 while(WaitForKey()!=27);	// wait for Esc
 ResetSprites(); 		// redefine all
 */
 Sprites_16();
	// sprite 2 (left 32 shifted)
 for(i=0;i<16;i++) mySprite2LineColours[i]|=SPRITE_COL_EC;
 
 SpritePattern( nrPattern2, Sprite32bytes(mySprite2Pattern) );
 SpriteColours( nrSpr2, mySprite2LineColours );

	// sprite 3
 SpritePattern( nrPattern3, mySprite3Pattern );
 SpriteColours( nrSpr3, mySprite3LineColours );
 
 k=0; x=0; y=0; dx=1; dy=1;
 
 PutText(0,176,"Press arrow keys,\0");
 PutText(0,190,"or Spacebar to exit\0");
 xi=170;
 yi=100;
 
 Sprites_On();			// Activate sprites
 while(k!=KB_SPACE)	// wait for Enter
  {   
   k=keyboard_read();	//Inkey() does glitches;
   
   if(k==KB_LEFT) xi--;		// left key pressed
   if(k==KB_RIGHT) xi++;	// right key pressed
   if(k==KB_UP) yi--;		// up key pressed
   if(k==KB_DOWN) yi++;		// down key pressed
   
   if(xi<=50) xi++;
   if(xi>=240) xi--;	// stay in box
   if(yi<=10) yi++;
   if(yi>=170) yi--;
   
   SpriteAttribs( nrSpr2, nrPattern2, xi, yi );
   
   SpriteAttribs( nrSpr3, nrPattern3, x, y );		// move and put sprite
   gx = xi-32; gy = yi;
   gx = (gx>=x ? gx-x : x-gx);
   gy = (gy>=y ? gy-y : y-gy);
   gx = (gx>=16 ? 1 : 0);
   gy = (gy>=16 ? 1 : 0);
   if(gx|gy) { x+=dx; y+=dy; }
   if(x<=0) dx=1;
   if(x>=223) dx=-1;
   if(y<=0) dy=1;
   if(y>=180) dy=-1;

   // to slow down MSX
   //_ei_halt();
  };

 SetScreen0(); 
 Restore_VDP();		// Restore VDP internals
}

	
