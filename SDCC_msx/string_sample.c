#include "solidc\CONIO.h"
#include "solidc\STRING.h"

//	replaces all sub-strings s1 with s2 in string adr,
//	uses memory right below code (heap_top=length of program+few bytes)
//	that should be free of data or code loaded after at runtime 

extern unsigned char *heap_top;

extern	void	strreplstr( char *adr, char *s1, char *s2 )
 {
 int i=0, l1 = strlen(s1);
 unsigned char *p = (void *)heap_top;		// free memory space below code
 
 while(1)
	{
	i = strstr(adr,s1);
	if(i<0) break;
	strncpy(p,adr,i);
	strcat(p,s2);
	strcat(p,adr+i+l1);
	strcpy(adr,p);
	}
 }
 

#define SBUF 0x9D00
char __at (0x9A00) s1[] = "Hello \0";
char __at (0x9B00) s2[] = "world of \0";
char __at (0x9C00) sn[] = "\n\0";
char __at (SBUF) sbuf[0xff];

void main(void)
{
 int n;
 strcpy( sbuf, s1 );		// Store string s1 to sbuf
 strcat( sbuf, s2 );		// Concatenate s2 to sbuf
 strncat( sbuf, s1, 4 );	// Concatenate first 4 chars to sbuf
 cputs( sbuf );  putch(10);
 strreplstr( sbuf, "ll\0",  "lio\0"); 	// string replace by string
 cputs( sbuf );  putch(10);
 strcat( sbuf, sn );		// Concatenate \n
 strreplstr( sbuf, "lio\0",  "ll\0"); 	// replace back
 cputs( sbuf );
 strncpy( sbuf, s1, 4 );	// Store 4 chars of s1 to sbuf = "Hell\0"
 n = strlen(sbuf);
 putdec(n); putch(',');		// print len of string in sbuf = 4
 n = strcmp(sbuf,s1);		// compare "Hell\0" and "Hello \0"
 putdec(n); putch(',');
 n = strncmp(sbuf,s1,4);	// compare first 4 chars of "Hell\0" and "Hello \0"
 putdec(n); putch(',');
 n = strstr(s1,sbuf);		// finds substring sbuf in s1
 putdec(n); putch(',');
 n = strchr(sbuf,'e');		// finds the first occurrence of 'e' in "Hell\0"
 putdec(n); putch(',');
 n = strrchr(sbuf,'l');		// finds the last occurrence of 'l' in "Hell\0"
 putdec(n); putch(',');
 n = strpbrk(sbuf,"abcdef\0");	// finds the position in sbuf of first char from the list "abcdef"
 putdec(n); putch(10);
 strcat( sbuf, sn );		// concatenate \n
 strupr(sbuf );			// to uppercase
 cputs( sbuf );
 strlwr(sbuf );			// to lowercase
 cputs( sbuf );
 strcpy(sbuf,"   :)   \0");
 strltrim(sbuf); strrtrim(sbuf);	// remove spaces in both sides
 cputs( sbuf ); cputs( "-<\0");
 strcpy( sbuf, s2 );
 strreplchr( sbuf, 'o', '0' );		// replaces all characters in string
 cputs( (char *)SBUF );
}
