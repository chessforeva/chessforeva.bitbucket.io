/*

	This is MSX1 sample of graphics in SCREEN 2
	(uses assembler graphics library for .COM files)
*/
#include "solidc\VDPgraph1.h"

#define	_pt_(h,v) PSET(X+(h),Y+(v),colour)
#define	_ln_(h,v) LINE(X+(h),Y+(v),X-(h),Y+(v),colour)
#define	pt_(h,v) if(OP==0xff)_ln_(h,v);else if(OP!=0xff)_pt_(h,v)

	// circle, add FILL_ALL to fill
extern	void	CIRCLE( int X, int Y, int Radius, int colour, int OP )
	{
	int x = Radius; int y = 0; int e = 0;
	while(1)
		{
		pt_(+x,+y); pt_(+x,-y); pt_(-x,+y); pt_(-x,-y);
		pt_(+y,+x); pt_(+y,-x); pt_(-y,+x); pt_(-y,-x);						
		if (x <= y) break;
		e += (y<<1) + 1; y++;
		if (e > x) { e += 1 - (x<<1); x--; }
		}
	}
	
// one of [0..255] patterns
#define nrPattern1 #0
char __at (0x9000) mySprite1Pattern[]= {
	0b11111111,
	0b11111111,
	0b11000011,
	0b11011011,
	0b11011011,
	0b11000011,
	0b11111111,
	0b11111111
};
// one of [0..32] sprites made of patterns
#define nrSpr1 #0


#define nrSpr2 #0
#define nrPattern2 #0
	// in 16 size mode, for sprite n  , set the (n*4) palette number  
unsigned int __at (0x9010) mySprite2Pattern[]= {
	0b1111111111111111, 
	0b1111111111111111,
	0b1100000000000011,
	0b1100000000000011,
	0b1100010110100011,
	0b1100001111000011,
	0b1100011111100011, 
	0b1111111111111111,
	0b1111111111111111, 
	0b1100011111100011,
	0b1100001111000011,
	0b1100000110000011,
	0b1100000110000011,
	0b1100001001000011,
	0b1111111111111111, 
	0b1111111111111111
};
#define nrSpr3 #1
#define nrPattern3 #4

// buffer for block we will save from screen
char __at (0xA000) pxpatterns[400];	// 64px x 50 rows
char __at (0xA200) pxcolors[400];

void main(void)
{
 int g;
 pxColor C;		// structure for colours of 8-pixel palettes
 
 Save_VDP();		// Save VDP internals
 SetScreen2();		// SET SCREEN 2
 SetColors(WHITE,BLACK,BLACK);	// white on black with black border
 ClearScreen();			// clear and make it black
 SpritesDoubleSized();		// Set large sprites
 /*
 Sprites_8();
 SpritePattern( nrPattern1, mySprite1Pattern );		// define new points set
	// Put a sprite to (40,120), linked to pattern
 SpriteAttribs( nrSpr1, nrPattern1, 40, 120, CYAN );
 */
 Sprites_16();
 SpritePattern( nrPattern2, Sprite32bytes(mySprite2Pattern) ); 
 SpriteAttribs( nrSpr2, nrPattern2, 40, 50, LIGHT_YELLOW | SPRITE_COL_EC );	// left 32px shifted
 
 EnableScreen();

 getCol8px(255,191, &C );	// get colours of 8-pixels palette 
 C.col=LIGHT_YELLOW;		// change colours
 C.bg=DARK_BLUE;
 setCol8px(255,191, &C );	// set colours to 8-pixels palette 
 
 set1px(255,191);		// set the last pixel of screen
 set1px(254,191);		// ... and also pre-pixel
 clear1px(254,191);		// clear pre-pixel
 g = get1px(254,191);		// is pixel set?
 if(g!=0) set8px(255,190);	// if set then set all 8-pixels of previous line
 
 PSET(0,0,MAGENTA);		// set pixel, set colour of palette
 g=POINT(0,0);			// get colour (=MAGENTA) of pixel of palette

 LINE(1,1,100,190,DARK_BLUE );
 LINE(200,190,1,1,DARK_BLUE );
 LINE(200,100,90, 30,DARK_RED );
 LINE(2,2,2,190,DARK_RED );
 LINE(2,2,255,2,DARK_RED );
 LINE(80,180,10,160,DARK_RED );
 LINE(220,20,10,160,DARK_RED );

 RECT(2,2,20,100,MEDIUM_RED, FILL_ALL);	// filled rectangle
 RECT(170,90,250,180,GRAY, NO_FILL);	// rectangle
 
 LINE(220,120,10,120,LIGHT_GREEN);
 
 // draw DONKEY classic (IBM,1981)
 DRAW("S16C3BM44,28M+2,-4R8M+1,-1U1M+1,+1M+2,-1M-1,+1M+1,+3M-1,+1M-1,-2M-1,+2D3L1U3M-1,+1D2L1U2L3D2L1U2M-1,-1D3L1U5M-2,+3\0");
 PAINT( 60,20,6 );
 
 Read_Block( 40,0,64,50, 0xA000, 0xA200 );	// read cow to memory 
 Write_Block( 8,80,64,50, 0xA000, 0xA200 );	// copy cow to new place 
 
 CIRCLE(120,120,40,LIGHT_YELLOW, NO_FILL);

 CIRCLE(180,40,20,DARK_GREEN, FILL_ALL);	// filled circle
 
 Read_Scr( 0x5000, 0x7000 );	// Read palettes data and colours data to VRAM
 
 PAINT( 180,100,MEDIUM_GREEN);
 ClearScreen();
				// restore screen as it was before painting and clearing
 Write_Scr( 0x5000, 0x7000 );	// Write palettes data and colours data to VRAM
 PAINT( 180,100,DARK_BLUE);
 
 SetColors(WHITE,BLACK,BLACK);	// white on black with black border
 PutText(0,156,"MSX1! Press ESC,");
 PutText(0,168,"return to MSXDOS A>\0");
 
 while(WaitForKey()!=27);	 // ESC key returns to MSXDOS
  
 SetScreen0();		// SET SCREEN 0 text mode
 SetColors(WHITE,DARK_BLUE,DARK_BLUE);	// restore default MSX colours
 ClearScreen();		// A>_
 Restore_VDP();		// Restore VDP internals
}
