#include "solidc\CONIO.h"
#include "solidc\DOS.h"

char __at (0x9000) sbuf[0xff];
char __at (0x9C00) sn[] = "\n\0";
char __at (0x9C10) sdow[] = ".day of week\n\0";
char __at (0x9CF0) stest[] = "CALL 5 test!\n$\0";

TIME __at (0x2100) tm;
DATE __at (0x21F0) dt;

void main(void)
{
 REGS *rr = _REGs();		// obtain address of structure 
 
 // display current date
 getdate(&dt);
 putdec(dt.year); putch('.');
 putdec(dt.month); putch('.');
 putdec(dt.day); putch(' ');
 putdec(dt.dow); cputs(sdow);
  
 // display current time
 gettime(&tm);
 putdec(tm.hour); putch(':');
 putdec(tm.min); putch(':');
 putdec(tm.sec); cputs(sn);
 
 rr->bc = 0x0009;	// c=9h print string ending with "$"
 rr->de = 0x9CF0;	// address of text to display
 intdos();
 
 rr->af = 'A';
 rr->iy = *( (unsigned int*)0xFCC0 );	// get main rom slot
 rr->ix = 0x00A2;	// display character
 intbios();
}
