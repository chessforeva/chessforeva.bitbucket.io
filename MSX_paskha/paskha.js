//
// Paskha engine - user control, drawing in canvas
// Uses other functions that are defined outside
// 

WD = 12;    // width

TOUCH_ = ('ontouchstart' in document.documentElement);
MOBILE_ = (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i));

DEMO_level1 =
   [1,1213,30,39,32,38,22,39,16,37,13,32,27,39,13,32,56,38,21,38,19,39,7,39,5,39,7,32,37,39,18,39,6,
    39,5,39,26,32,12,39,20,39,14,37,8,37,6,37,16,38,19,38,32,37,6,38,42,38,37,37,14,37,12,37,12,37,18,32,13,37,
    10,37,21,39,16,38,22,38,41,39,3,38,36,38,21,38,19,39,20,37,15,32,38,37,18,39,18,39,17,38,30,39];
__u = "undefined";

BESTS = []; // best walkthrough from server

if(typeof(REC)==__u)
 {
 REC = [];   // current recording to play
 }
  
I = { f:[], img:[] };

O = { S:1 /*scaling*/, lvl:1 /*level*/, doFocus:1, demo:0, scrlX:0, scrlY:0, key:[], fk:0,
      fimg:0, loops:0, PI2:Math.PI*2, toScroll:0, musics:0, stopped:0,
      
 empty0: [0, 0xd8, 0xd0, 0xd1, 0xd2, 0xd3, 0xf0 ],
 bricks: [0x1,0x2,0x3,0x4,0x5,0x6,0x8,0x84,0xb,0xba,0xc,0xe0,0xe4,0xe8,0xec],
 enemy0: [0xf1,0xf3,0xf4,0xf5,0xf6,0xfa,0xfb,0xfc],
 goodie: [0xe4, 0xec, 0xe0, 0xe8],
 goodi2: [0xd3, 0xd2, 0xf0, 0xd1],
 mouse0: [0xff, 0xfe, 0xfd, 0xf8, 0xf9, 0xef, 0xee, 0xed],
 fades0: [0x4a, 0x4b, 0x4c, 0x4d],

 chn: [[0x1,-12,-1],[0x2,+12,-1],[0x3,-1],[0x4,-12,+1],[0x5,-12],[0x6,-1,+1],
       [0x8,+12,+1],[0x84],[0xe0],[0xe4],[0xe8],[0xec],[0xb,-12,+12],[0xba,+12],[0xc,+1]],

 cntrlk: [75,76,188,77,190,191,32,13,68, 90,88,40,112],

 bored: 0xA0,
 rec:[]
 };

//small update feb.2023 for touchscreens
betweenbricks = { r:0, c:0 };		// case when jumps between bricks
farjump = { r:0, c:0 };				// case when jumps far 2 spaces and up
//-----------------------

if(!O.fimg) LoadImages();
 
// When canvas is ready and images are loaded
function StartGM()
{
StartLevel(0);
}

function goLvl(n) { O.demo = 0; goLevel(n); }
function goLevel(n) { O.lvl = n; REC = []; StartGM(); }

function StartLevel(plus)
{
O.lvl = min_( O.lvl+plus, 24); 
O.scrlX = 0; O.scrlY = 0; O.wait = 0; O.wt = 0;
O.died = 0; O.dt = 0; O.d2 = 36; O.made = 0;
O.key = []; O.fk = 0; O.j =1; O.jump = 0; O.ljump = 0; O.lj2 = 0;
O.move = 0; O.beat = 0; O.bt = -1; O.upr = 0; O.bott = 0; O.score = 0;
O.fade = 0; O.bea2 = 0; O.fall = 0; O.c = 0; O.r = 0; O.jy = 0; O.jx = 0; O.bo = 0;
O.tick = 0; O.rtck = 0; O.rec = []; O.rp = 0; O.en = []; O.door = 0;

if(O.demo && O.lvl==1 && REC.length==0) REC=DEMO_level1;

_dispCurLvl();

SC();

_upd_pillars();
_upd_bottles();
drawLevel();
_redraw();
_reSzScl();

if(!O.loops)
 {
  // simple way but click is not ok
  if(TOUCH_)
   {
   O.cs.addEventListener("touchstart", mouseClick, false);
   O.cs.addEventListener("touchmove", DummyEv, false);
   O.cs.addEventListener("touchend", DummyEv, false);
   }
  else
   {
   O.cs.addEventListener("mousedown", mouseClick, false);
   }
  document.addEventListener("keydown", keyClick, false);
  document.addEventListener("keypress", keyNope, false);
  document.addEventListener("keyup", keyNope, false);  
  MKeyLoop(); O.loops = 1;
 }
}

function DummyEv(e) { e.preventDefault(); e.stopPropagation(); return false; } 
      
function GE(id) { return document.getElementById(id); }
function int_(x) { return parseInt(x); }
function abs_(x) { return Math.abs(x); }
function sgn_(a) { return (a==0?0:(a>0?1:-1)); } // if no inbuilt sign function
function max_(a,b) { return Math.max(a,b); }
function min_(a,b) { return Math.min(a,b); }

function n_(r,c) { return ((WD*r)+c); }
function _nr(n) { return int_(n/WD); }
function _nc(n) { return (n%WD); }


// on loading
function onLoad()
{
 O.cs = GE('paskhaCanvas');
 if(O.cs==null) setTimeout('onLoad()',1000);       // reload if canvas not ready
 else
  {
   O.cx = O.cs.getContext('2d');
   LdVrfImg();
  }
}


// on loading define all image sources
function LoadImages()
{
 var j,z,i;
 for(i=0; i<=0xff; i++) I.f[i]=0;
 for(j in level)
  {
   z = level[j];
   for(i in z) _ldImg(z[i]);
  }
 z = [ 0xdc ].concat(O.empty0).concat(O.bricks).concat(O.enemy0)
            .concat(O.goodie).concat(O.goodi2).concat(O.mouse0).concat(O.fades0);
 for(i in z) _ldImg(z[i]);
 O.fimg=1;
}

function _ldImg(i)
{
 if(!I.f[i])
  {
  n = iByCode(i);
  I.img[i] = new Image();
  var s = "b" + n.toString(16) + ".png";
  I.img[i].src = s;
  document.write('<img src='+s+' id="imgs_'+i+'" width="0" height="0">');
  I.f[i] = 1;
  }
}

// verify all images for readyness
function LdVrfImg()
{
 var i,f=1;
 for(i=0; i<=0xFF; i++)
   if(I.f[i] && (!IsImgLoaded(I.img[i]))) f=0; 
 setTimeout( (f ? 'StartGM()' : 'LdVrfImg()' ),500);
}

// detects if image is loaded and ready for displaying
function IsImgLoaded(img)
{
 return !((!img.complete) || (typeof img.naturalWidth !== __u && img.naturalWidth === 0));
}

function iByCode(n)
{
 var f,t,i;
 f=[ 0, 0x10, 0xb6, 0xa6, 0xa8, 0xbc, 0x8a, 0x8e, 0xae, 0xa2, 0x93, 0x97, 0xc3, 0xcb, 0x9b, 0x9e, 0xb2 ];
 t=[ 0xa, 0xa, 0xba, 0xba, 0x3, 0x3, 0xba, 0xba, 0xba, 0xba, 0xc, 0xc, 0xc, 0xc, 0xc, 0xba, 0xba  ];
 for (i in f) if(f[i]==n) return t[i];
 return n;
}

// goodie inside
function i84(n) { return ( isinlist(O.goodie,n) ? 0x84 : n); }

function s84(n)
{
 var f=O.goodie, t=O.goodi2;
 for (i in f) if(f[i]==n) return t[i];
 return 0;
}

// Rows in level
function LvlRows()
{
 var k,j,r=0,c=0,o;
 o = level[ O.lvl ];
 for(k in o)
  {
  j = o[k];
  if(j==0x10) break;
  c++;
  if(c==WD) { r++; c=0; }
  }
 return r;
}

function isinlist(arr, c)
{
 var i,f=0;
 for(i in arr) if(arr[i]==c) { f=1; break; }
 return f;
}

// Is free to move
function is_E(r,c) { return isinlist( O.empty0, Ch(r,c) ); }
function isR(r,c) { return (!is_E(r,c)); }

// is brick
function is_B(r,c) { return isinlist( O.bricks, iByCode(Ch(r,c))); }

// is goodie
function is_G(r,c) { return isinlist( O.goodie, Ch(r,c) ); }
function is_g2(r,c) { return isinlist( O.goodi2, Ch(r,c) ); }

function isDC(r,c) { return (Ch(r,c)==0xDC); } // blue fixed
function is0(r,c)  { return (Ch(r,c)==0); }     // empty
function isD8(r,c) { return (Ch(r,c)==0xD8); }  // door to exit level
function isD0(r,c) { return (Ch(r,c)==0xD0); }  // column pillar
function isD1(r,c) { return (Ch(r,c)==0xD1); }  // door through enemies
function isD2(r,c) { return (Ch(r,c)==0xD2); }  // bottle
function isD3(r,c) { return (Ch(r,c)==0xD3); }  // column to take
function isF0(r,c) { return (Ch(r,c)==0xF0); }  // diamond

// falling objects
function is2fall(r,c) { return (!(is0(r,c) || isDC(r,c) || is_M(r,c))); }

// enemies
function is_M(r,c) { return isinlist( O.enemy0, Ch(r,c) ); }

function Ch(r,c) { return O.L[ n_(r,c) ]; }
function setCh(r,c,v) { O.L[ n_(r,c) ] = v; }

// Redraws level in canvas
function drawLevel()
{
 var k,j,r=0,c=0;
 O.rwcnt = LvlRows();
 O.cs.width = O.S * WD * 36;
 O.cs.height = O.S * O.rwcnt * 36;
 O.L = level[ O.lvl ].slice();
 _Modified(); // add few bricks
 O.died = 0;
 
 for(k in O.L)
  {
  j = O.L[k];
  if(j==0x10) break;
  if(j==0xff)
   {
    O.r=r; O.c=c;
    setCh(r,c,0);
   }

  if(isinlist(O.enemy0,Ch(r,c))) { j=0; setCh(r,c,0); }
  DrwImg( i84(j), c*36,r*36, 36, 36 );
  c++;
  if(c==WD) { r++; c=0; }
  }
 scrollMe();
}

// columns of me
function C1() { return O.c - (O.jx<0 ? 1 : 0); }
function C2() { return O.c + (O.jx>0 ? 1 : 0); }
// rows of me
function R1() { return O.r - (O.jy>0 ? 1 : 0); }
function R2() { return O.r + (O.jy<0 ? 1 : 0); }

function _forceDie() {  O.key.push(68); }

// Main loop, user controls
function MKeyLoop()
{
 var o,v,p,u,z,m,f,key;
 
 if(O.doFocus) O.cs.focus();
 O.tick++;
 
 // push recorded in keyboard list
 _ToPlayRecKey();
  
 key = O.key[0]
       
 O.bo++;
 if(O.bea2>0) O.bea2--;
  
 DM(0);
 
 u = O.jy;
 z=1;
 
 if(O.jump)
  {
  if(O.jy==0)
   {
   if(O.g>0 && ( isR(R1()-1,C1()) || isR(R1()-1,C2()) )) z=0;
   if(O.g>0 && isDC(R1()-1, (O.j<0 ? C1()-1 : C2()+1)) )
    {
     z=0;
     if(!O.fk)
      {
      O.key = [(O.j<0 ? 188 : 190)].concat(O.key);
      O.fk=1;
      }
    }
   if(O.g<0 && ( isR(R2()+1,C1()) || isR(R2()+1,C2()) )) O.jump=0;
   if(!O.demo && O.g<0 && O.jump && betweenbricks.r == O.r && betweenbricks.c == O.c+sgn_(O.j))	// included new feature
	  {
		z=0;
		if(!O.fk)
			{
			O.key = [(O.j<0 ? 188 : 190)].concat(O.key);
			O.fk=1;
			}
		O.jump = 0; O.move = 1;
	  }
   }
  }
 else
  {
  if( !(O.died || O.made) && is_E(O.r+1,C1()) && is_E(O.r+1,C2()) )
   {
    O.g = -1;
    O.jump = 1;
    O.fk = 1;
   }
  }

 if(z && O.jump)
  {     
  v = O.jy+O.g;

  if((O.jump) && (O.move) && (O.jy!=0) && abs_(v)<12
     && (sgn_(O.g)==-sgn_(O.jy)) && is_E(O.r,(O.j<0?C1()-1:C2()+1)) ) v=0;
 
  
  if(abs_(v)>=36 || sgn_(v)!=sgn_(O.jy))
   {
    if((!O.fk) && (O.jy!=0))
      {
      O.key = [(O.j<0 ? 188 : 190)].concat(O.key);
      O.fk=1;
      }
    if(O.g>0)
     {
     if( isR(R1()-1,C1()) || isR(R1()-1,C2()) )
      {
       if(abs_(v)<36) v=0;
       v = sgn_(v)*36;
      }
     } 
    if(O.g<0)
     {
     if( isR(R2()+1,C1()) || isR(R2()+1,C2()) )
      {
       if(abs_(v)<36) v=0;
       v = sgn_(v)*36;
       O.jump = 0;
      }
     }
    }

  O.jy = v;

  if((v!=0) && abs_(v)>=36)
   {
    p = sgn_(v);
    O.r-=p; O.jy-=p*36;
    if( O.fk && is_E(O.r,O.c+sgn_(O.j)) ) O.jy=0;
   }
  
  }

 if(O.jump)
  {
   if(O.g>-8)
    {
     if(O.ljump)
      {
      O.lj2++;
      if(O.lj2==5) O.lj2=0;
      }
     if(O.lj2==0) O.g--;
    }
  }
 else O.g=0;

 if(O.beat || O.bt<0)
  {
   O.bt++;
   if(O.bt==0) O.beat=1;
  }

 if(!(O.died || O.made) && abs_(O.jx)>=36 )
  {
  O.move=0;
  if(O.jx>0) O.c++; else O.c--;
  O.jx=0;
  }
  
 if(O.move && (O.jx==0))
  {
   p=( O.j > 0 ? C2()+1 : C1()-1 );
   if((O.jump) && (O.jy!=0) && abs_(O.jy)<12 && (sgn_(O.g)==-sgn_(O.jy)) )
    {
    if(isR(O.r,p)) { O.fk=0; O.move=0; }
    }
   else
    {
     if(isR(R1(),p) || isR(R2(),p)) { O.fk=0; O.move=0; }
    }
  }
  
 if(!(O.died || O.made) && O.move) { O.jx+=O.j; O.fk=1; }
 
 if(!(O.died || O.made) && (O.jx==0) && (O.jy==0) && (!O.move) && (!O.jump))
  {
  if(isD8(O.r,O.c))
   {
    O.made = 1;
   }
  }

 if(O.died || O.made)
 {
  KR();
 }
 else
 {
 if(key == 68 || key==112) // [d][F1]
  {
  if(!O.made) { O.died = 1; O.dt = 36; }
  KR();
  }
 
 if(key == 90 || key == 88 || key == 40) //[z][x] [Down]
  {
  if(O.upr>0 && is0(O.r,O.c))
   {
    setCh(O.r,O.c,0xD0); _clr(O.r,O.c);
    O.upr--; _upd_pillars();
    O.bt = -8;
   }
   KR();
  } 
 
 if (key == 37)     // [Left]
  {
   if((!O.move) && O.j>0) { O.j = -4; KR(); }
   else key = 188;
  }
 if (key == 39)     // [Right]
  {
   if((!O.move) && O.j<0) { O.j =4; KR(); }
   else key = 190;
  }
 if (key == 38)     // [Up]
  {
   key = (O.j<0 ? 75 : 76);
  }
 
 if (key == 75 || key == 76)  // Up [k][l]
  {
   if(O.jump) key = (key == 75 ? 188 : 190);
   else
    {
     if(!O.move)
      {
      O.j = (key==75? -4 : 4);
      O.jump = 1;
      O.g = ( O.ljump ? 10 : 10 ); O.jy = 0; KR();
      }
    }
  }
 
 if (key == 188 || key==77)   // Left [<][m]
  {
   if(!O.move)
    {
    O.j = -4;
    if(O.c==0 && O.jx==0) KR();
    else{ O.move = 1; KR(); }
    }
  }
 if (key == 190 || key==191)  // Right [>][?]
  {
  if(!O.move)
   {
   O.j = 4;
   if(O.c==(WD-1) && O.jx==0) KR()
   else { O.move = 1; KR(); }
   }
  }
 if (key == 32 || key==13)  // Space,Enter
  {
  if(!(O.jump || O.move || O.fade))
   {
    var c1 = O.c+sgn_(O.j);
    if(O.jx!=0 || c1<0 || c1>=WD || (!is_B(O.r, c1))) KR();
    else
     {
     f=1;
     if(O.fall)
      {
       for(m=0;m<O.FL.length;m++)
         {
         if(isinlist(O.FL[m], n_(O.r, c1))) f=0;
         }
       }
     if(f)
      {
       O.beat = 1; O.bt = 0; O.bea2 = 8; O.fade=1; prepToFade(); O.score+=20; SC();
      }
     KR();
     }
   }
  }

 // if keys not expected then remove
 if(key>0)
  {
  if(!isinlist(O.cntrlk,key)) KR();
  else scrollMe();
  }

 }

 if(O.fade) fadeBricks();
 if(O.beat) fallingBlocks(); // falling bricks

 if(!O.wait)
  {
  if(O.died)
   {
    if(O.dt>2) O.dt-=2;
    else {  O.wt = 66; O.wait = 1; }
   }
   if(O.made) { O.wt = 66; O.wait = 1; _playNextLevel(); }
  }
 else
  {
   if(O.wt>0) O.wt--;
   if(O.wt==0)
    {
     if(O.demo) { O.demo=0;  O.died=1; }
     else
      if(O.made) maybeRecord();
     REC = [];
     StartLevel((O.died ? 0 : 1));
    }
  }
 
 if(!O.jump) {
	O.fk=0;
	if(!O.move) {
		betweenbricks = { r:0, c:0 };
		farjump = { r:0, c:0 };
	}
 }
   
 _Goodies();
 _Enemies();
 _redraw(); // updates visual glitches
 DM(1);
 
 if(O.stopped) O.loops = 0;
 else setTimeout("MKeyLoop()",30);
}


function scrollMe()
{
 if(O.toScroll)
 {
 if( ((O.r-4)<O.scrlY) || ((O.r+5)>O.scrlY) || ((O.c-6)<O.scrlX) || ((O.c+5)>O.scrlX) )
  {
   O.scrlY = O.r;
   O.scrlX = O.c;
   window.scrollTo( (max_(0,O.c-4)*36*O.S), (max_(0,O.r-2)*36*O.S) );
  }
 }
}

// this prepares blocks to fade
function prepToFade()
{
var n,a = block_(O.r, O.c+sgn_(O.j));
O.fB = [];
for(i in a)
 {
 n = a[i];
 O.fB.push( { k:0, n:n, r: _nr(n), c: _nc(n), w:O.L[n] } );
 O.L[n]=0;
 }
}

function isinlistFb(n)
{
 var i,f=0;
 for(i in O.fB)
  if(O.fB[i].n==n) { f=1; break; };
 return f; 
}


// processes falling of blocks
function fallingBlocks()
{
var w=1,r,c,n,a,i,j,q,_r,_c,f,o,b,z,m;

for(;w==1;)
{
w=0;
if(!O.fall)
 {
  O.FL = [];
  r=0;c=0;n=0;
  for(i in O.L)
  {
   if(is2fall(r,c))
     {
      a = block_(r,c); f=1;
      for(j in a)
       {
        q = a[j]; _r = _nr(q); _c = _nc(q);
        b = a.slice(0,j);
        if((isD0(_r+1,_c) || isR(_r+1,_c) ||
            isinlistFb(q+WD) || isD8(_r+1,_c)) && (!isinlist(b,q+WD))) f=0;
       }
      if(f)
       for(m=0;m<O.FL.length;m++)
         {
         if(isinlist(O.FL[m], a[0])) f=0;
         }

      if(f)
       { 
        O.FL.push(a); O.fall = 1; O.ft = 0; w=1;
       }
     }         
  c++;
  if(c==WD) { r++; c=0; }
  n++;
  }
 if(!O.fall) O.beat = 0;
 }
else
 {
  O.ft+=4;
  if(!w)
   {
   for(i in O.FL)
   {
    o = O.FL[i];
    for(j in o)
     {
      q = o[j]; r = _nr(q); c = _nc(q);
      DrwImg( 0, (c*36),(r*36)+O.ft-4, 36, 36 );
     }
    z=[];
    for(j in o)
     {
      q = o[j]; r = _nr(q); c = _nc(q); m = O.L[q]; z.push(m);
      if(is_B(r,c) && ((C1()==c)||(C2()==c)))
       {
        var uy = (O.r*36)-O.jy, y2 = ((r+1)*36)+O.ft, ux = (O.c*36)+O.jx, x2 = (c*36);
        if((uy<y2) && (uy+36)>y2)
         {
          O.jy-=(y2-uy);
          var p,v = O.jy;
          if((v!=0) && abs_(v)>=36)
            {
            p = sgn_(v);
            O.r-=p; O.jy-=p*36;
            }
         }
        if(O.move && (O.jx==0) && abs_(uy-y2)<=36) O.move=0;
       }
      DrwImg( i84(m), (c*36),(r*36)+O.ft, 36, 36 );
     }
    if(O.ft==36)
     {
     for(j in o)
      {
      q = o[j]; r = _nr(q); c = _nc(q);
      if(!isD0(r+1,c)) { O.L[q+WD]=z[j]; O.L[q]= 0; }
      }
      w=1;     
     }
   }
   if(w)
    {
     O.fall = 0; O.FL = []; O.ft = 0;
    } 
   }
 }

}
}

// gets array of block bricks
function block_(r,c)
{
var a=[ n_(r,c) ];
var t,n,h,i,o,j,w;
for(t=0;t<a.length;t++)
 {
  n = a[t]; h = iByCode(O.L[n]); 
  for(i in O.chn)
  {
  o = O.chn[i];
  if(o[0]==h)
    {
    for(j in o)
     if(j>0)
      {
       w = o[j];
       if((WD!=12) && (abs_(w)==12)) w=sgn_(w)*WD;
       k=n+w; if(!isinlist(a,k)) a.push(k);
      }
    }
  }
 }
 return a;
}

// fading bricks
function fadeBricks()
{
 var i,n,w,o,f=0,j;
 for(i in O.fB)
  {
  o = O.fB[i];
  j = int_(o.k/6);
  n = (j<4 ? 0x4a+j: 0);
  if(n==0)
   {
    w = s84(o.w);
    O.L[o.n] = w;
   }
  else w = n;
      
  DrwImg( w, o.c*36,o.r*36, 36, 36 );
  if(n) o.k++; else f=1;
  }
 if(f) { O.fade = 0; O.fB=[]; O.bt = -1; }
}

function dbg(s) { GE('dbgDiv').innerHTML=''+s; }

// draw mouse image
function DM(c)
{
var n = 0, d = (O.j>0 ? 0x10 : 0 ), f=0, bg = Ch(O.r,O.c);

if(c)
 {
  f=_drawInBg();
  if(O.bea2) n=0xed+d;
  else
   {
    var u = (O.jx&8)>>3;
    u|=((O.jump && abs_(O.j)>3)?1:0);
    n=0xef+d-u; // moving 
    if(O.bo>O.bored) n=0xf9;
    if(O.bo>O.bored+0x0C) n=0xf8;
    if(O.bo>O.bored+0x0F) O.bo=0;
    if(O.mode || O.died) n=0xf9;
   }
 }

if(f)
 {
 O.cx.globalAlpha = 0.7;
 DrwImg( bg, (O.c*36),(O.r*36), 36, 36 );
 }
var k1 = (O.dt>0 ? 36-O.dt : 0);
DrwImg( n, (O.c*36)+O.jx,(O.r*36)-O.jy+k1, 36, dieY() );
if(f) O.cx.globalAlpha = 1;
}

function dieY()
{
 var r=O.d2,r2=R2();
 if(O.dt>0)
  {
   O.d2 = min_(O.dt,O.d2);
   O.made = 0;
   return O.dt;
  }
 if(isR(r2,C1()) || isR(r2,C2()))
  {
   r = (r2*36) - ((O.r*36)-O.jy);
   if(r<32)
    {
    r = min_(O.d2,max_(r,1));
    O.d2 = r;
    O.died = 1;
    O.jump = 0;
    O.move = 0;
    O.made = 0;
    _DieTick = 0;
    }
  }
 return r;
}

// clear/redraw at
function _clr(r,c)
{
 DrwImg( Ch(r,c), (c*36),(r*36), 36, 36 );
 _drwpoint(r,c);
}


function _drawInBg()
{
 var n = n_(O.r,O.c), z = O.L[n], f =(z!=0);
 return f;
}


function _Goodies()
{
 var f,i,j,r,c,ca = [C1(),C2()], ra = [R1(),R2()];
 for(i in ca)
  for(j in ra)
   {
    r = ra[j]; c = ca[i];
    if((j==0 || r!=ra[0]) && (i==0 || c!=ca[0]))
    {
    f=0;
    if( isD3(r,c) ) // take column
        {
        O.upr+=2; _upd_pillars(); O.score+=40; SC();
        f=2;
        }
    if( isD2(r,c) ) // take bottle
        {
        O.bott++;
        if(O.bott==3) O.ljump = 1; O.score+=60; SC();
        _upd_bottles();
        f=2;
        }
    if( isF0(r,c) ) // diamond
        {
        O.score+=100; SC();
        f=2;
        }
    if( isD1(r,c) ) // door
        {
        O.door = 1; _upd_doors();
        f=1; //redraw only
        }        
    if(f)
      {
       if(f==2) setCh(r,c,0);
       _clr(r,c);
      }
    }
   }
}

function _Enemies()
{
 var i,o;
 if(!(O.made || O.died))
 {
 for(i in O.en)
 {
  o=O.en[i];        // enemies not moving
  if(o.f && (!is0(o.yp/36,o.xp/36))) o.f=0;
  if(o.f)
   {
    DrwImg(o.k,o.xp,o.yp,36,36); 
    if(!O.door)
    {
    y0 = (O.r*36)-O.jy;
    x0 = (O.c*36)+O.jx;
    if(abs_(x0-o.xp)<=28 && abs_(y0-o.yp)<=28) { O.died = 1; O.dt = 36; }
    }
   }
 }
 }
}

function _redraw()
{
 if((!O.tick) || (O.tick&0x10))
 {
  if(!(O.jump || O.move || O.fall || O.fade))
  {
  var i,c=0,r=0;
  for(i in O.L)
   {
    if( isD0(r,c) ) _clr(r,c);
    if( is0(r,c) ) _drwpoint(r,c);
    c++;
    if(c==WD) { c=0; r++; }
   }
  }
 }
}

function _drwpoint(r,c)
{
 if( is0(r,c) )
 {
 O.cx.beginPath();
 O.cx.strokeStyle = "#660033";
 O.cx.fillStyle = O.cx.strokeStyle;
 O.cx.arc( O.S*((c*36)+18),O.S*((r*36)+18),O.S, 0, O.PI2 );
 O.cx.lineWidth = O.S;
 O.cx.stroke();
 }
}

// remove key from memory
function KR()
{
O.key.splice(0,1);
O.bo=0;
}

// Draw scaled image
function DrwImg(i,x0,y0,w,h)
{
if(!(typeof(I.img[i])==__u)) O.cx.drawImage(I.img[i], O.S*x0, O.S*y0, O.S*w, O.S*h);
}

//window.onkeydown = function (e)
function keyClick(e) {
    var code = e.keyCode ? e.keyCode : e.which;
    _appKeyCode(code);
    e.preventDefault();
    e.stopPropagation();
};

function keyNope(e) { e.preventDefault(); e.stopPropagation(); }

function __X(o) { return ( typeof(o.clientX)==__u ? o.pageX : o.clientX ); }
function __Y(o) { return ( typeof(o.clientY)==__u ? o.pageY : o.clientY ); }

function mouseClick(e)
{
 var X,Y;
 if(e.type == 'touchstart' || e.type == 'touchmove' || e.type == 'touchend' || e.type == 'touchcancel')
  {
   var touch = event.touches[0];
   if(!(typeof(touch)==__u)) { X = __X(touch); Y = __Y(touch); }
   else
    {
    touch = e.originalEvent.touches[0];
    if(!(typeof(touch)==__u)) { X = __X(touch); Y = __Y(touch); }
    else { X = __X(e); Y = __Y(e); }
    }
  }
 else if (e.type == 'mousedown' || e.type == 'mouseup' || e.type == 'mousemove' ||
     e.type == 'mouseover'|| e.type=='mouseout' || e.type=='mouseenter' || e.type=='mouseleave')
  { X = __X(e); Y = __Y(e); }
  
 var rect = O.cs.getBoundingClientRect();
 X-=rect.left;
 Y-=rect.top; 
 
 var up=0, rep=0, code = 0, c2=0, q=O.j;
 
 X/=O.S; Y/=O.S;
 var x0 = ((36*O.c)+O.jx+18), y0 = ((36*O.r)-O.jy+18);
 var dy = abs_(Y-y0), dx = abs_(X-x0);
 var row = ((Y/36)|0), col = ((X/36)|0);
 
 if(dy<100)
  {
   if(dx<16 && dy<16) O.bo=O.bored+0x0C;
   else
    {
    O.j = (X>=x0 ? 4 : -4);

    if(Y<(y0-16) && dx<200)
     {
      code = (dx<60 ? 38 : ( O.j<0 ? 75 : 76 ));
     }
    else
      {
      if(!(O.jump||O.move) && dy<18 && dx<90 &&
         ((O.j>0 && X>x0 && is_B(O.r,O.c+1)) ||
            (O.j<0 && X<x0 && is_B(O.r,O.c-1))) ) code = 32;
      else
       if(O.upr>0 && dx<36 && Y>(y0+30)) code = 90;
       else
        {
         if(dx>16) code = ( O.j<0 ? 188 : 190 );
         if(dy<20)
          {
           if(dx>52) rep++;
           if(dx>88) rep++;
          } 
        }
      }

    if(code==0) O.j=q;  // restore
    }
  }
  
  if(code)
   {
	if( row>=0 && row<O.rwcnt && col>=0 && col<WD ) {
		if( is_E(row,col) && !is_E(row+1,col) && !is_E(row-1,col) ) {
			betweenbricks = { r:row, c:col };
			}
		}
		var m = sgn_(O.j);
		if( ( (col == O.c+m+m+m) && !is_E(O.r+1,O.c) && is_E(O.r+1,O.c+m) && is_E(O.r+1,O.c+m+m) ) ||
			( (col == O.c+m+m) && ((row == O.r-2) || (row == O.r-1) )) ) {
			farjump = { r:row, c:col };
			code = ( O.j<0 ? 188 : 190 );		// move and then jump
			_appKeyCode( code,1);
			code = 38;
			up = 1;
		}	
	
     if(!up && code==38 && is_E(O.r+1,O.c+sgn_(O.j)))
       {
        code = ( O.j<0 ? 188 : 190 );
        up = 1;
       }
       
    _appKeyCode(code | (q==-O.j ? 0x100 : 0),1 );
		

    if(up) { code=38; rep=1; }

    if(code==32 && dx>54)
     {
      code = ( O.j<0 ? 188 : 190 );     // run fast
      rep=2;
     }
    for(;rep>0;rep--) { if(rep>0) _appKeyCode(code,1); }
   }
   
  event.preventDefault();

}

function _appKeyCode(c,f)
{
 if(c && O.demo) { _DieTick = 0; O.demo=0; goLevel(1); }
 if((c) && REC.length==0 && (f || O.key.length<2))
   {
   var r=O.rec, l=r.length;
   r.push( { key:c, tick:O.tick } );
   O.key.push(c & 0xff);
   }
}

function _reSize(t)
{
 var k,j,r=0,c=0;
 O.S = int_(t/4)/10;
 O.cs.width = O.S * WD * 36;
 O.cs.height = O.S * O.rwcnt * 36;
 
 for(k in O.L)
  {
  j = O.L[k];
  if(j==0x10) break;
  DrwImg( i84(j), c*36,r*36, 36, 36 );
  c++;
  if(c==WD) { r++; c=0; }
  }
 _reSzScl();
  scrollMe();
}


function prepStrOfRec()
{
 var i,o,d,r=O.rec, s=''+O.lvl+","+O.score;
 for(i in r)
  {
   o = r[i]; d = o.tick-(i==0?0:r[i-1].tick);
   s+= ","+d+","+o.key
  }
 return s;
}

function _ToPlayRecKey()
{
 var l=REC.length,i,dt,key,t;
 if(l)
  {
    for(;O.rp<l; O.rp+=2)
    {
    i=O.rp;
    dt = REC[i];
           // Skip long thinking waiting....
    if(!(O.fall || O.fade || O.jump || O.move)) dt = min_(dt,300);
    key = REC[i+1];
    t = O.rtck + ((i>0)?dt:0);
    if(t>O.tick) break;
    if(key & 0x100) { key &= 0xff; O.j = -O.j; }
    O.key.push(key);
    O.rtck = t;
    }
  }
 if((!O.demo) && (!l) && (O.rec.length==0) && (O.tick>0x150) && (O.lvl==1)) { O.demo=1; goLevel(1); }

}
